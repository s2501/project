$(document).ready(function(){

    // на всички линкове, им задаваме фунционалност
    // в къдравите скоби, при събитието click
    $('.links-in-portfolio').on("click", function(){

        // присвояваме на target, string, който е стойноста на
        // атрибута data-open, на елемента от навигацията
        // върху който сме кликнали

        console.log(this);

        var target = $(this).attr('data-open');

        // $('.'+target) == html element избран с jquery

        // махаме active на всички елементи, които държат съдържание
        $( ".content-container" ).removeClass( "active" );

        // добавяме active клас на селектираният елемент
        $('.'+target).addClass( "active" );


        $( ".links-in-portfolio" ).removeClass( "active-link" );
        $( this ).addClass( "active-link" );
    });

});

var param = 'global param';

var dropdown = function() {

    var desktop = true;
    var mobile = false;

    function init() {
        console.log(desktop);
    }

    return init();
}

var app = {};

var ux = app.ux = {

    config: {
        desktop: true,
        mobile: false
    },

    init: function(){
        console.log(this.config.desktop);
    }
}