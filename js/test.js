//
// типове променливи
//
var param = 123.50; // това е присвояване на числена стойност

var paramNotNumber = "123.50"; // това е присвояване на стойност от тип стринг
var param2 = "some text and numbers 1,2,3"; // това е присвояване на стойност от тип стринг

var flag = true; // това е присвояване на стойност от булеви тип
var flag2 = false; // this is negative булеви value

var someParam; // това е недефиниране променлива

var ivan, // отново недефинирани променливи
    pesho;

var gosho = null; // променлива, която няма никаква присвоена стойност

var arrayVar = [1,2,3,4,5];  // променлива от тип едномерен масив или array
// викаме стойност от масив с:
// иметоНаПроменливата[index]

var arrayVar2 = [1,2,"123",4,5]; // променлива от тип едномерен масив или array

var objectVar = { // обект в JavaScript
    property1: "123",
    property2: "456",
    property3: "789"
};
// викаме стойност от обект с:
// иметоНаПроменливата.ключа

var complexObject = { // обект в JavaScript с функция
    zulum: function(){
        console.log(complexObject.stoinost);
    },
    stoinost: '3'
};
// викаме функция от обект с:
// иметоНаПроменливата.ключа()

var arrayWithObjects = [ // масив изграден от обекти
    {
        property1: "some value",
        property2: "another value"
    },
    {
        testKey1: "testing",
        testKey2: "testing again"
    }
];
// викаме стойност от масив с обекти чрез:
// иметоНаМасива[index].ключа

// селектиране на елемент с Vanilla JavaScript
var someHTMLElement = document.querySelector('.heading-cta');
// селектиране на елемент с jQuery
var selectItWithjQuery = $('.heading-cta');

function panaiot(){  // изписване на функция
    console.log("извиках функцията Иван");
}

// panaiot();  // извикване на функция

var pesho = function(){ // изписване на функция присвоена на променлива
    console.log("извиках функцията Пешо");
};

// pesho(); // извикване на функция

var selfCalling = function(){ // самоизвикваща се функция
    console.log("самоизвикваща се функция");
}();

// ------------------------------------------
var a = 5;
var b = 6;

// математическа функция
function shaSmqtame(arg1, arg2) {
    var c = arg1 + arg2;
    console.log(c);
}
// shaSmqtame(a, b);

// функция, която ни връща стойност
function returnTrueAlways() {
    return true;
}

// функция за сравнение
function sravnenie(arg1, arg2) {
    if (arg1 > arg2){
        console.log(arg1 + " е по-голямо от " + arg2);
    } else {
        console.log(arg2 + " е по-голямо от " + arg1);
    }
}

// FizzBuzz function
// Нужно е да има HTML елементи за да работи
function fizzbuzz () {
    var counter = parseInt(document.querySelector('.fizz-buzz-number').value, 10);
    var palnej = document.querySelector('.fizz-buzz');
    palnej.innerHTML = "";
    var fizzCounter = 0,
        buzzCounter = 0,
        fizzBuzzCounter = 0;
    var a = [];
    for (var i = 0; i < counter; i++) {
        a.push(i+1);
    }
    for (var y = 0; y < a.length; y++) {
        if (((a[y] % 3) === 0) && ((a[y] % 5) == 0)) {
            palnej.innerHTML += 'Fizz Buzz, ';
            fizzBuzzCounter++;
        } else if ((a[y] % 3) == 0) {
            palnej.innerHTML += "Fizz, ";
            fizzCounter++;
        } else if ((a[y] % 5) == 0) {
            palnej.innerHTML += "Buzz, ";
            buzzCounter++;
        } else
        palnej.innerHTML += a[y] + ", ";
    }

    palnej.innerHTML += "<br> fizzBuzzCounter: " + fizzBuzzCounter +
                        "<br> fizzCounter: " + fizzCounter +
                        "<br> buzzCounter: " + buzzCounter;
}